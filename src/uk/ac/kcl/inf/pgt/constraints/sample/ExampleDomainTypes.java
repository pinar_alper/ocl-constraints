/**
 * 
 */
package uk.ac.kcl.inf.pgt.constraints.sample;

import java.util.HashMap;
import java.util.Map;

import uk.ac.kcl.inf.pgt.model.QName;
import uk.ac.kcl.pgt.constraints.beans.VarDatatypeEnum;

/**
 * @author palper
 *
 */
public class ExampleDomainTypes {

	public static Map<QName, VarDatatypeEnum> typeMap;

	
	static {

		typeMap = new HashMap<QName, VarDatatypeEnum>();
		typeMap.put(new QName("ex","date"),VarDatatypeEnum.DATE);
		typeMap.put(new QName("ex","diagnosisKind"),VarDatatypeEnum.STRING);
		typeMap.put(new QName("ex","treatmentKind"),VarDatatypeEnum.STRING);

		typeMap.put(new QName("graph","name"),VarDatatypeEnum.STRING);

	}


}
