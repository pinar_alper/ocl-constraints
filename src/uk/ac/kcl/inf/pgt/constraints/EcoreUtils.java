/**
 * 
 */
package uk.ac.kcl.inf.pgt.constraints;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.Map;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EcoreFactory;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;

import uk.ac.kcl.pgt.constraints.beans.VarDatatypeEnum;

/**
 * @author palper
 *
 */
public class EcoreUtils {

	public static File createSimpleModelWithAttributes(Map<String, VarDatatypeEnum> templateVars) throws IOException {
	
		EcoreFactory ecoreFactory = EcoreFactory.eINSTANCE;
		EPackage ePackage = ecoreFactory.createEPackage();
		ePackage.setName("testPackage");
		ePackage.setNsPrefix("testPkg");
		ePackage.setNsURI("http://uk.ac.kcl.inf.pgt");

		EClass eClass = ecoreFactory.createEClass();
		eClass.setName("template");
		/* Add to the EPackage's EClassifiers */
		/* EClasses and EDatatypes implement both EClassifiers */
		ePackage.getEClassifiers().add(eClass);

		templateVars.forEach((k, v) -> {

			EAttribute eAttrib = ecoreFactory.createEAttribute();
			eAttrib.setName(k);
			eAttrib.setEType(CspToOclTypeMap.typeMap.get(v));

			eClass.getEStructuralFeatures().add(eAttrib);

		});

		/* Initialize the EPackage */
		ePackage.eClass();
		Resource.Factory.Registry reg = Resource.Factory.Registry.INSTANCE;
		Map<String, Object> m = reg.getExtensionToFactoryMap();
		/* add default .ecore extension for ecore file */
		m.put(EcorePackage.eNAME, new XMIResourceFactoryImpl());

		// Obtain a new resource set
		ResourceSet resSet = new ResourceSetImpl();
		// create a resource
		Resource resource = null;

			resource = resSet.createResource(URI.createFileURI("temporary.ecore"));
	
		/*
		 * add the EPackage as root, everything is hierarchical included in
		 * this first node
		 */
		resource.getContents().add(ePackage);

		// now save the content.
		resource.save(Collections.EMPTY_MAP);

		File result =  new File(resource.getURI().path());
		
		System.out.println("Converting template and variables to a temporary ecore  @" + result.getAbsolutePath());

		return result;
	}

}
