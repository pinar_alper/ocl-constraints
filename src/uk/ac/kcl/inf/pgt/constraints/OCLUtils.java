/**
 * 
 */
package uk.ac.kcl.inf.pgt.constraints;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EObject;

import tudresden.ocl20.pivot.essentialocl.expressions.impl.ExpressionInOclImpl;
import tudresden.ocl20.pivot.essentialocl.expressions.impl.IntegerLiteralExpImpl;
import tudresden.ocl20.pivot.essentialocl.expressions.impl.IteratorExpImpl;
import tudresden.ocl20.pivot.essentialocl.expressions.impl.OperationCallExpImpl;
import tudresden.ocl20.pivot.essentialocl.expressions.impl.PropertyCallExpImpl;
import tudresden.ocl20.pivot.essentialocl.expressions.impl.TypeLiteralExpImpl;
import tudresden.ocl20.pivot.essentialocl.expressions.impl.VariableExpImpl;
import tudresden.ocl20.pivot.essentialocl.expressions.impl.VariableImpl;
import tudresden.ocl20.pivot.pivotmodel.Constraint;

/**
 * @author palper
 *
 */
public class OclUtils {

	public static String toString(EObject eo) {

		String notfound = "not found ";

		if (eo instanceof OperationCallExpImpl) {

			OperationCallExpImpl oCEI = (OperationCallExpImpl) eo;

			return oCEI.getReferredOperation().getName().toString();

		} else if (eo instanceof PropertyCallExpImpl) {

			PropertyCallExpImpl oCEI = (PropertyCallExpImpl) eo;

			return oCEI.getReferredProperty().getName().toString();

		} else if (eo instanceof VariableExpImpl) {

			VariableExpImpl oCEI = (VariableExpImpl) eo;

			return oCEI.getReferredVariable().getName().toString();

		} else if (eo instanceof TypeLiteralExpImpl) {

			TypeLiteralExpImpl oCEI = (TypeLiteralExpImpl) eo;

			return oCEI.getReferredType().getName().toString();

		} else if (eo instanceof IntegerLiteralExpImpl) {

			IntegerLiteralExpImpl oCEI = (IntegerLiteralExpImpl) eo;

			return oCEI.getIntegerSymbol() + "";

		} else if (eo instanceof VariableImpl) {

			VariableImpl oCEI = (VariableImpl) eo;

			return oCEI.getType().toString();

		} else if (eo instanceof IteratorExpImpl) {

			IteratorExpImpl oCEI = (IteratorExpImpl) eo;

			return oCEI.getName();

		} else if (eo instanceof ExpressionInOclImpl) {

			ExpressionInOclImpl oCEI = (ExpressionInOclImpl) eo;

			return oCEI.getBodyExpression().toString();
			// ;

		}

		return notfound + "   " + eo.getClass();

	}

	public static void prettyPrintAST(Constraint c) {
		Map<EObject, Integer> depths = new HashMap<EObject, Integer>();

		for (TreeIterator<EObject> allContents = c.eAllContents(); allContents.hasNext();) {

			EObject content = allContents.next();
			EObject parent = content.eContainer();
			if (depths.containsKey(parent)) {
				depths.put(content, depths.get(parent) + 1);
			} else {
				depths.put(content, 0);
			}
			System.out.println(i2s(depths.get(content)) + toString(content));

		}
	}

	private static String i2s(Integer integ) {
		String res = "";
		for (int i = 0; i < integ; i++) {
			res += " + ";
		}
		return res;
	}

}
