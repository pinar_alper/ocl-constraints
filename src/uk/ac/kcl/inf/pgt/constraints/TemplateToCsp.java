/**
 * 
 */
package uk.ac.kcl.inf.pgt.constraints;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import tudresden.ocl20.pivot.model.IModel;
import tudresden.ocl20.pivot.pivotmodel.Constraint;
import tudresden.ocl20.pivot.standalone.facade.StandaloneFacade;
import uk.ac.kcl.inf.pgt.constraints.sample.ExampleDomainTypes;
import uk.ac.kcl.inf.pgt.model.QName;
import uk.ac.kcl.inf.pgt.model.Template;
import uk.ac.kcl.inf.pgt.model.Vertex;
import uk.ac.kcl.inf.pgt.prov.ProvExport;
import uk.ac.kcl.inf.pgt.prov.ProvUtils;
import uk.ac.kcl.pgt.constraints.beans.CspSolution;
import uk.ac.kcl.pgt.constraints.beans.DomainXmlConfigReader;
import uk.ac.kcl.pgt.constraints.beans.VarDatatypeEnum;
import uk.ac.kcl.pgt.constraints.beans.Variable;
import uk.ac.kcl.pgt.constraints.choco.ChocoCSPInstance;
import uk.ac.kcl.pgt.constraints.register.Register;

/**
 * @author palper
 *
 */
public class TemplateToCsp {

	private ChocoCSPInstance inst = null;

	public TemplateToCsp(File templateFile, File configFile, File oclFile) throws Exception {

		super();

		Template t = new Template(ProvExport.parseGraph(new FileInputStream(templateFile), ProvUtils.FORMAT_PROVN));

		File modelFile = getTemplateAsEcoreModel(t);

		// Read the variables that are subjected to constraint and their 
		// domain information from an XML config file
		// This file also contains distribution constraints those denoting the  frequencies 
		// with which variables take on (particular) values from their domains
		DomainXmlConfigReader reader = new DomainXmlConfigReader(configFile);

		//The domains are comprised of values of String and Date type.
		//these are mapped to integers, as the CSP only supports in variables.
		//this mapping is kept in the Register singleton.
		Register.getInstance().initialize(reader.getVariables(), reader.getDistributions());

		// Create a Choco CSP Model with variable definitions and distribution
		// constraints obtained from
		// the Configuration XML file.
		inst = new ChocoCSPInstance(reader.getNumInstances(), Register.getInstance());

		List<Constraint> cons = parseOclConstraints(modelFile, oclFile);


		SimpleOclExpHandler handler = new SimpleOclExpHandler(inst);

		//For each contraint traverse its syntax tree and simultaneous create a corresponding CSP constraint
		//within Choco CSP.
		for (Constraint c : cons) {

			handler.traverseAST(c);

		}

	}

	public void run() {

		if (inst != null) {
			CspSolution[] results = inst.solve();
			for (CspSolution sol : results) {

				Map<Variable, String> map = sol.getVariableValueMap();

				for (Map.Entry<Variable, String> entry : map.entrySet()) {

					System.out.println(entry.getKey().getName() + "\t \t " + entry.getValue());

				}
			}
		} else
			throw new RuntimeException();
	}

	private File getTemplateAsEcoreModel(Template t) throws IOException {

		Set<Integer> verticeKeys = t.getVertices();

		Iterator<Integer> itr = verticeKeys.iterator();

		Map<String, VarDatatypeEnum> templateVars = new HashMap<String, VarDatatypeEnum>();

		while (itr.hasNext()) {

			Vertex vertex = t.getVertex(itr.next());

			// TODO add support for constraints over zone variables.
			if (!inZone(vertex.getProperties())) {

				for (Map.Entry<QName, QName> varEntry : t.getPlaceheldNames(vertex).entrySet()) {

					QName varPlace = varEntry.getValue();
					QName var = varEntry.getKey();

					// TODO:
					// HERE WE SHOULD GO TO THE DOMAIN's NAMESPACE (recall prefix ex <http://example.com/>  from
					// the example. Instead we use the ExampleDomainTypes class.
					
					// The domain namespace point to an XSD or OWL based  model that
					// outlines attribute/property types.
					// We need this information to figure out the type of values that are value variables
					// will take.
				
					VarDatatypeEnum phType = ExampleDomainTypes.typeMap.get(varPlace);
					templateVars.put(var.getValue(), phType);

				}

			}

		}

		// Creation of the temporary model file
		// We need this model file because the Dresden OCL API requires an
		// associated model when loading an OCL file.
		return EcoreUtils.createSimpleModelWithAttributes(templateVars);

	}

	private boolean inZone(Set<QName> props) {

		boolean result = false;
		for (QName prop : props) {

			if (prop.inNamespace("zone"))
				result = true;

		}

		return result;
	}

	public static void main(String[] args) {
		try {

			File templatesFile = new File("resources/templates/myTest.provn");

			File oclFile = new File("resources/constraints/tappUseCase17.ocl");

			File configFile = new File("resources/constraints/Tapp2017UseCaseDomain.xml");

			TemplateToCsp t2c = new TemplateToCsp(templatesFile, configFile, oclFile);

			t2c.run();
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private List<Constraint> parseOclConstraints(File modelFile, File oclFile) throws Exception {

		StandaloneFacade.INSTANCE.initialize(new URL("file:" + new File("log4j.properties").getAbsolutePath()));

		IModel model = StandaloneFacade.INSTANCE.loadEcoreModel(modelFile);

		List<Constraint> constraintList = StandaloneFacade.INSTANCE.parseOclConstraints(model, oclFile);

		for (Constraint c : constraintList) {

			OclUtils.prettyPrintAST(c);

		}

		return constraintList;

	}

}
