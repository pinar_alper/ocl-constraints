/**
 * 
 */
package uk.ac.kcl.inf.pgt.constraints;
/*
 * Copyright (C) 2008-2011 by Claas Wilke (claas.wilke@tu-dresden.de)
 *
 * This file is adapted from  OCL2Java Code Generator of Dresden OCL.
 *
 * Dresden OCL is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Lesser General Public License as published by the 
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Dresden OCL is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along 
 * with Dresden OCL. If not, see <http://www.gnu.org/licenses/>.
 */

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.HashSet;
import java.util.Set;

import org.apache.log4j.Logger;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

import tudresden.ocl20.pivot.essentialocl.expressions.CollectionLiteralExp;
import tudresden.ocl20.pivot.essentialocl.expressions.ExpressionInOcl;
import tudresden.ocl20.pivot.essentialocl.expressions.StringLiteralExp;
import tudresden.ocl20.pivot.essentialocl.expressions.IntegerLiteralExp;
import tudresden.ocl20.pivot.essentialocl.expressions.OperationCallExp;
import tudresden.ocl20.pivot.essentialocl.expressions.PropertyCallExp;
import tudresden.ocl20.pivot.essentialocl.expressions.impl.CollectionItemImpl;
import tudresden.ocl20.pivot.essentialocl.expressions.util.ExpressionsSwitch;
import tudresden.ocl20.pivot.pivotmodel.Constraint;
import tudresden.ocl20.pivot.pivotmodel.Expression;
import tudresden.ocl20.pivot.pivotmodel.Operation;
import tudresden.ocl20.pivot.tools.codegen.code.ITransformedCode;
import uk.ac.kcl.pgt.constraints.choco.ChocoCSPInstance;

/**
 * <p>
 * This class provides handler methods for those nodes in the OCL Syntax tree
 * that are relevant for the provenance simulator.
 * </p>
 * 
 * @author palper
 * 
 */
public class SimpleOclExpHandler extends ExpressionsSwitch<String> {

	/** The {@link Logger} for this class. */
	private static final Logger LOGGER = Logger.getLogger(SimpleOclExpHandler.class);


	Deque<Object[]> argumentStack;

	private ChocoCSPInstance csp;
	

	public SimpleOclExpHandler(ChocoCSPInstance instance) {
		
		super();

		csp = instance;
		
		argumentStack = new ArrayDeque<Object[]>();

	}

	
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * tudresden.ocl20.pivot.essentialocl.expressions.util.ExpressionsSwitch
	 * #caseExpressionInOcl
	 * (tudresden.ocl20.pivot.essentialocl.expressions.ExpressionInOcl)
	 */
	public String caseExpressionInOcl(ExpressionInOcl anExpressionInOcl) {

		System.out.println("****** caseExpressionInOcl : ");

		/* Transform bodyCode. */
		String result = doSwitch(anExpressionInOcl.getBodyExpression());

		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * tudresden.ocl20.pivot.essentialocl.expressions.util.ExpressionsSwitch
	 * #caseIntegerLiteralExp
	 * (tudresden.ocl20.pivot.essentialocl.expressions.IntegerLiteralExp)
	 */
	public String caseIntegerLiteralExp(IntegerLiteralExp anIntegerLiteralExp) {

		System.out.println("****** anIntegerLiteralExp : " + OclUtils.toString(anIntegerLiteralExp));

		argumentStack.push(csp.chocoIfyIntegerConstant(new Integer(anIntegerLiteralExp.getIntegerSymbol()).intValue()));
		
		return OclUtils.toString(anIntegerLiteralExp);
	}

	/**
	 * <p>
	 * Generates the code for a binary {@link Operation} of an
	 * {@link OperationCallExp}.
	 * </p>
	 * 
	 * @param anOperationCallExp
	 *            The {@link OperationCallExp} the code shall be transformed
	 *            for.
	 * @param anOperation
	 *            The {@link Operation} which shall be transformed.
	 * @return The {@link ITransformedCode} for the {@link Operation} which
	 *         shall be transformed.
	 */
	public String caseOperationCallExp(OperationCallExp anOperationCallExp) {

		System.out.println("****** caseOperationCallExp : " + anOperationCallExp.getReferredOperation().getName());

		EList<EObject> contents = anOperationCallExp.eContents();

		for(EObject o:contents){
			doSwitch(o);
		}
		
		String opName = anOperationCallExp.getReferredOperation().getName();

		if (CspToOclTypeMap.isArithmetic(opName)){
			
			Object[] rhs = argumentStack.pop();
			Object[] lhs = argumentStack.pop();
			
			argumentStack.push(csp.chocoIfyArithmetic(lhs,opName, rhs));
			
			
		}else if (CspToOclTypeMap.isComparative(opName)){
		
			Object[] rhs = argumentStack.pop();
			Object[] lhs = argumentStack.pop();
		
			csp.submitRelationConstraint(lhs, opName, rhs); reset();
		
			//TODO Check with test data if I still need to do a reset...
			//might no longer be necessary
			
		}else if (CspToOclTypeMap.isImplication(opName)){
			Object[] rhs = argumentStack.pop();
			Object[] lhs = argumentStack.pop();
			
			csp.submitValueDependencyConstraint(lhs, rhs); reset();
			
		}else if (CspToOclTypeMap.isInclusion(opName)){
			Object[] rhs = argumentStack.pop();
			Object[] lhs = argumentStack.pop();
			
			argumentStack.push(csp.chocoIfySetInclusion(lhs, rhs));
		}


		return anOperationCallExp.getReferredOperation().getName();
	}

	private void reset() {
		argumentStack.clear();
		
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * tudresden.ocl20.pivot.essentialocl.expressions.util.ExpressionsSwitch
	 * #casePropertyCallExp
	 * (tudresden.ocl20.pivot.essentialocl.expressions.PropertyCallExp)
	 */
	public String casePropertyCallExp(PropertyCallExp aPropertyCallExp) {

		System.out.println("****** casePropertyCallExp : " + OclUtils.toString(aPropertyCallExp));

		/* Get parameters for property call code. */
		String referredProperty = aPropertyCallExp.getReferredProperty().getName();

		argumentStack.push(csp.chocoIfyVariable(referredProperty));

		return OclUtils.toString(aPropertyCallExp);
	}

	

	public String caseCollectionLiteralExp(CollectionLiteralExp aCollectionLiteralExp){
		
		System.out.println("****** caseCollectionLiteralExp : " + aCollectionLiteralExp);

		EList<EObject> contents = aCollectionLiteralExp.eContents();

		Set<String> strSet = new HashSet<String>();
		for(EObject o:contents){
			//System.out.println(o);
			for(EObject oo:((CollectionItemImpl)o).eContents()){
				strSet.add(((StringLiteralExp)oo).getStringSymbol());
			}
			
		}
		argumentStack.push(csp.chocoifyStringSet(strSet));
		
		return null;
	}
	
	public void traverseAST(Constraint aConstraint) throws Exception {

		Expression anExpression = aConstraint.getSpecification();
		doSwitch(anExpression);

	}


}