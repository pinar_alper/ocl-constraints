/**
 * 
 */
package uk.ac.kcl.inf.pgt.constraints;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EcorePackage;

import uk.ac.kcl.pgt.constraints.beans.VarDatatypeEnum;

/**
 * @author palper
 *
 */
public class CspToOclTypeMap {

	public static Map<VarDatatypeEnum, EDataType> typeMap;
	public static Set<String> booleanOclOperators;
	public static Set<String> arithmeticOclOperators;

	static {

		typeMap = new HashMap<VarDatatypeEnum, EDataType>();
		typeMap.put(VarDatatypeEnum.STRING, EcorePackage.eINSTANCE.getEString());
		typeMap.put(VarDatatypeEnum.DATE, EcorePackage.eINSTANCE.getEBigInteger());
		typeMap.put(VarDatatypeEnum.INTEGER, EcorePackage.eINSTANCE.getEBigInteger());

		booleanOclOperators = new HashSet<String>();
		booleanOclOperators.add(">");
		booleanOclOperators.add(">=");
		booleanOclOperators.add("<");
		booleanOclOperators.add("<=");
		booleanOclOperators.add("<>");
		booleanOclOperators.add("=");

		arithmeticOclOperators = new HashSet<String>();
		arithmeticOclOperators.add("+");
		arithmeticOclOperators.add("-");
		arithmeticOclOperators.add("*");
		arithmeticOclOperators.add("/");

	}

	public static boolean isArithmetic(String op) {
		return arithmeticOclOperators.contains(op);

	}

	public static boolean isComparative(String op) {
		return booleanOclOperators.contains(op);
	}

	public static boolean isImplication(String op) {
		return op.equals("implies");
	}

	public static boolean isInclusion(String opName) {
		return opName.equals("includes");
	}
}
